\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{framabook}[2007/12/03]

%-----------------------------------------------------------------------
%
% Avec des si...
%
\newif\ifversionenligne
\newif\ifbrouillon
\newif\ifencouleur
%
\encouleurtrue
\brouillonfalse
\versionenlignetrue

%-----------------------------------------------------------------------

%-----------------------------------------------------------------------
%
% Declaration des options
%
\DeclareOption{draft}{\brouillontrue\PassOptionsToClass{draft}{book}}
\DeclareOption{versionpapier}{\versionenlignefalse} 
\DeclareOption{versionenligne}{\versionenlignetrue}
\DeclareOption{couleur}{\encouleurfalse}
\DeclareOption{nb}{\encouleurfalse}
\DeclareOption*{\PassOptionsToClass{\CurrentOption}{book}}
%
% Exécution des options
%
\ProcessOptions
%
\LoadClass[a4paper,twoside]{book}
%-----------------------------------------------------------------------


%----------------------------------------------------------------------
%
% les petits paquets à charger
%

% standard ------------------------------------------
\RequirePackage{fancybox}
\RequirePackage[utf8]{inputenc}
\RequirePackage[T1]{fontenc}
\RequirePackage{graphicx}
\RequirePackage{tikz}
\RequirePackage{index}
\usetikzlibrary{arrows,shadows}
\RequirePackage[scaled=0.78]{beramono}
\RequirePackage{kpfonts}
\RequirePackage{microtype}
\renewcommand{\ttdefault}{cmtt}
\renewcommand{\ttdefault}{cmtt}
\RequirePackage{ifthen}
\RequirePackage{ifpdf}
\RequirePackage{siunitx}
\RequirePackage{fancyvrb}
\RequirePackage{natbib}
\RequirePackage[french]{babel}
\RequirePackage{mykeys}
\RequirePackage{caption}
\RequirePackage{url}
\RequirePackage[french]{varioref}
\RequirePackage{subfig}
\RequirePackage{textcomp}
\RequirePackage{pifont}
\RequirePackage{etoolbox}
\RequirePackage[official]{eurosym}
\RequirePackage{listingsutf8}


\ifversionenligne % version en ligne
\newcommand\fmblinkcolor{blue!70!red}
 \RequirePackage[pdftex,
                 hyperindex=true,
                 pdfpagelabels,
                 bookmarksnumbered=true,
                 colorlinks=true,
                 linkcolor=\fmblinkcolor,
                 menucolor=\fmblinkcolor]{hyperref}
\else % version papier
 \RequirePackage[pdftex,
                 hyperindex=false,
                 colorlinks=true,
                 linkcolor=black,citecolor=black,
                 filecolor=black,urlcolor=black]{hyperref}
\fi

% maison -------------------------------------------
\RequirePackage{nota,citation}
\RequirePackage{sommaire}
\RequirePackage{manumac}
\RequirePackage{titlebox}
\RequirePackage{malettrine}
\RequirePackage{sql}
\RequirePackage{php}
\RequirePackage{bash}
\RequirePackage{pythonlst}
\RequirePackage{mcd-ea}
\RequirePackage{mr}
% spécifique framabook
\RequirePackage{onglets}
\RequirePackage{framasections}
\RequirePackage{framageometrie}
\RequirePackage{framaentetes}
\RequirePackage{framavoir}
\RequirePackage{glossaire}
\RequirePackage{mybook}
\RequirePackage{nota}
\RequirePackage{citation}
\RequirePackage{myminitoc}
\RequirePackage{framabook}

\captionsetup[figure]{%
  labelfont=bf, labelsep=endash,
  textfont=sl,
  font=small,
  singlelinecheck=off
}


%-----------------------------------------------------------------------
%
% emplacements des graphiques
%
\input{ousontmesimages}
%-----------------------------------------------------------------------


%-----------------------------------------------------------------------
% configuration de l'environnement nota
\newcommand{\ficnota}{fmb-important}
\newcommand{\ficnote}{fmb-note}
\newcommand{\ficpg}{pg_logo}
\newcommand{\ficnotahack}{fmb-question}
\newcommand{\ficstop}{fmb-stop}

\setlength{\largeurnota}{.8cm}
\newenvironment{nota}{%
  \begin{pictonote}{\ficnota}}{\end{pictonote}}
\newenvironment{attention}{\begin{nota}}{\end{nota}}
\newenvironment{note}{%
  \begin{pictonote}{\ficnote}}{\end{pictonote}}
\newenvironment{notahack}{%
  \begin{pictonote}{\ficnotahack}}{\end{pictonote}}
\newenvironment{notapg}{%
  \begin{pictonote}{\ficpg}}{\end{pictonote}}
\newenvironment{notastop}{%
  \begin{pictonote}{\ficstop}}{\end{pictonote}}


%-----------------------------------------------------------------------
% configuration des épigraphes
\setlength{\epigraphetopskip}{0pt}
\setlength{\epigraphebottomskip}{0.6cm}
%-----------------------------------------------------------------------


%-----------------------------------------------------------------------
% configuration de la minitoc
\setlength{\mymtcwidth}{.7\textwidth}
\setlength{\mymtcbottomskip}{.5\baselineskip}
\let\minisommaire\minitoc
\dominitoc


%-----------------------------------------------------------------------
% saut interparagraphe
\setlength{\parskip}{4pt plus 4pt minus 2pt}
%-----------------------------------------------------------------------

%-----------------------------------------------------------------------
%
% Francisation
%
\AddThinSpaceBeforeFootnotes % pour les puristes (donc tout le monde!) 
\frenchsetup{%
  ItemLabels=\textendash,
  AutoSpacePunctuation=false,
  FrenchFootnotes=true % pour les notes de bas de page
}
%-----------------------------------------------------------------------

%-----------------------------------------------------------------------
%
% espace verticale entre les groupes dans l'index
%
% \renewcommand\indexspace{\par \vskip 20pt plus5pt minus3pt\relax}

%-----------------------------------------------------------------------

%-----------------------------------------------------------------------
%
% pour changer le nom des chapitres en annexes
\let\appendixORIG\appendix
\renewcommand{\appendix}{%
  \appendixORIG%
  \renewcommand{\chaptername}{Annexe}% :-( ??
}

%-----------------------------------------------------------------------

%-----------------------------------------------------------------------
%
% pour avoir un lien correct dans les bookmark du pdf, sur l'index
%
\let\printindexORIG\printindex
\renewcommand{\printindex}{%
  \cleardoublepage
  \phantomsection% création d'une fausse section
  \addcontentsline{toc}{chapter}{Index}
  \printindexORIG}
%-----------------------------------------------------------------------


\AtBeginDocument{%
  \selectlanguage{french}%
  \sisetup{locale=FR}%
  \makeindex%
}

\AfterBeginDocument{%
  \fmb@ouverture\newpage}

\AtEndDocument{%
  %\newpage\fmb@fermeture%
}

%
% fin du fichier
%
\endinput

